//
//  ImageZoomViewController.swift
//  DafitiTest
//
//  Created by Carl Osorio on 04/09/17.
//  Copyright © 2017 Trainning. All rights reserved.
//

import UIKit
import Kingfisher

class ImageZoomViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var item: ShowItem?
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = image
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        item?.getPicture(quality: .fanart, completionHandler: { (url) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.imageView.kf.setImage(with: url, placeholder: self.image)
        })
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
