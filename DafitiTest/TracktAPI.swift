//
//  TracktAPI.swift
//  DafitiTest
//
//  Created by Carl Osorio on 31/08/17.
//  Copyright © 2017 Trainning. All rights reserved.
//

import UIKit
import Alamofire

class TracktAPI: NSObject {
    
    static func getCatalog(search: String? = "", completionHandler: @escaping (([ShowItem]) -> Void)) {
        
        Alamofire.request("https://api.trakt.tv/search/movie?query=\(search ?? "")", headers: ["trakt-api-key": "709a6fc3c36ce9427a50431e81b1d29d9ddf7cf32338c27a6d26f69f1cd53c4f", "Content-Type": "application/json"]).responseJSON { (answer) in
            
            if let allData = answer.value as? [[String: Any]] {
                
                let allItems = allData.map({ (dic) -> ShowItem in
                    return ShowItem(dictionary: dic)
                })
                
                completionHandler(allItems)
                
            }
            
        }
        
    }
    
    static func getFullMovie(slug: String, completionHandler: @escaping ((ShowItem) -> Void)) {
        
        Alamofire.request("https://api.trakt.tv/movies/\(slug)?extended=full", headers: ["trakt-api-key": "709a6fc3c36ce9427a50431e81b1d29d9ddf7cf32338c27a6d26f69f1cd53c4f", "Content-Type": "application/json"]).responseJSON { (answer) in
            
            if let dic = answer.value as? [String: Any] {
                
                completionHandler(ShowItem(dictionary: ["movie": dic]))
                
            }
            
        }
        
    }

}
