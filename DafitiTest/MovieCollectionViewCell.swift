//
//  MovieCollectionViewCell.swift
//  DafitiTest
//
//  Created by Carl Osorio on 03/09/17.
//  Copyright © 2017 Trainning. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var movieCover: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    
}
