//
//  ViewController.swift
//  DafitiTest
//
//  Created by Carl Osorio on 31/08/17.
//  Copyright © 2017 Trainning. All rights reserved.
//

import UIKit
import Kingfisher

class ViewController: UICollectionViewController, UISearchBarDelegate {

    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    
    var allItems: [ShowItem] = []
    var filteredItems: [ShowItem] = []
    
    var isSearching = false
    
    let searchBar = UISearchBar()
    
    let refresher: UIRefreshControl = {
        let refresher = UIRefreshControl()
        refresher.tintColor = UIColor.white
        refresher.addTarget(self, action: #selector(updateCatalog), for: .valueChanged)
        return refresher
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView!.addSubview(refresher)
        
        searchBar.delegate = self
        
        flowLayout.itemSize = CGSize(width: (view.frame.size.width / 2) - 5, height: (view.frame.size.width / 2) - 5)
        
        if tabBarController?.selectedIndex == 0 {
            updateCatalog()
        }
    }
    
    func updateCatalog() {
        TracktAPI.getCatalog(completionHandler: {
            self.allItems = $0
            self.refresher.endRefreshing()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: { 
                self.collectionView?.reloadData()
            })
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if tabBarController?.selectedIndex == 1 {
            self.navigationItem.rightBarButtonItems = nil
            if self.allItems.count != Favorites.mine.count {
                self.allItems = Favorites.mine
                collectionView?.reloadData()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let movieDetailVC = segue.destination as? MovieDetailViewController,
            let indexPath = collectionView?.indexPathsForSelectedItems?.first {
            
            movieDetailVC.show = isSearching ? self.filteredItems[indexPath.row] : self.allItems[indexPath.row]
        }
    }
    
}

// MARK: The search
extension ViewController {
    
    @IBAction func search(_ sender: Any) {
        self.navigationItem.titleView = searchBar
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Close"), style: .done, target: self, action: #selector(closeSearchBar))
        searchBar.becomeFirstResponder()
    }
    
    func closeSearchBar() {
        self.navigationItem.titleView = nil
        self.isSearching = false
        self.filteredItems = []
        self.collectionView?.reloadData()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(search(_:)))
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        TracktAPI.getCatalog(search: searchBar.text) { (search) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.isSearching = true
            self.filteredItems = search
            searchBar.resignFirstResponder()
            self.collectionView?.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        self.isSearching = false
        self.filteredItems = []
        self.collectionView?.reloadData()
        
    }
    
}

// MARK: The collection view
extension ViewController {

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if !isSearching {
            return self.allItems.count
        } else {
            return self.filteredItems.count
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var item = self.allItems[indexPath.row]
        
        if isSearching {
            item = self.filteredItems[indexPath.row]
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MovieCollectionViewCell
        
        cell.movieTitle.text = item.title
        cell.movieCover.image = #imageLiteral(resourceName: "PlaceholderImage")
        
        if let url = item.coverPicture {
            cell.movieCover.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "PlaceholderImage"), options: [.transition(.fade(0.3))])
        } else {
            item.getPicture { (url) in
                if let url = url {
                    cell.movieCover.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "PlaceholderImage"), options: [.transition(.fade(0.3))])
                }
            }
        }
        
        return cell
        
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

}

