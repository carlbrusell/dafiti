//
//  Favorites.swift
//  DafitiTest
//
//  Created by Carl Osorio on 04/09/17.
//  Copyright © 2017 Trainning. All rights reserved.
//

import UIKit

class Favorites: NSObject {
    
    static var userDefaults: UserDefaults {
        return UserDefaults.standard
    }
    
    static var mine: [ShowItem] {
        if let data = userDefaults.data(forKey: "myFavorites") {
            let favorites: [ShowItem]? = NSKeyedUnarchiver.unarchiveObject(with: data) as? [ShowItem]
            return favorites ?? []
        } else {
            return []
        }
    }
    
    static func isFavorite(_ item: ShowItem) -> Bool {
        
        var mine = Favorites.mine
        
        let filtered = mine.filter { (itemK) -> Bool in
            
            return itemK.id.imdb == item.id.imdb
            
        }
        
        return filtered.count != 0
        
    }
    
    static func removeFavorite(_ item: ShowItem) {
        
        var mine = Favorites.mine
        
        var itemSelected: Int = -1
        
        for (key, itemK) in mine.enumerated() {
            if itemK.id.imdb == item.id.imdb {
                itemSelected = key
            }
        }
        
        if itemSelected >= 0 {
            mine.remove(at: itemSelected)
        }
        
        let data = NSKeyedArchiver.archivedData(withRootObject: mine)
        
        userDefaults.set(data, forKey: "myFavorites")
        userDefaults.synchronize()
    }
    
    static func addToFavorite(_ item: ShowItem) {
        
        if item.isFavorited {
            return
        }
        
        var mine = Favorites.mine
        mine.append(item)
        
        let data = NSKeyedArchiver.archivedData(withRootObject: mine)
        
        userDefaults.set(data, forKey: "myFavorites")
        userDefaults.synchronize()
        
    }
    
}
