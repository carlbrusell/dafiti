//
//  Movie.swift
//  DafitiTest
//
//  Created by Carl Osorio on 03/09/17.
//  Copyright © 2017 Trainning. All rights reserved.
//

import UIKit

class ShowItem: NSObject, NSCoding {
    
    let id: (imdb: String, slug: String, tmdb: String, trakt: String)
    let title: String
    let year: Int
    let type: ShowType
    
    /** Optional items
     The item bellow descripted are nil when the show is incomplete.
     This happens when the object is created on the catalog/search screen.
     When you open up the product detail, the other data is loaded.
     */
    
    var tagLine: String?
    var overview: String?
    var released: Date?
    var trailer: String?
    var rating: Double?
    var votes: Int?
    var homepage: String?
    
    var coverPicture: URL?
    
    init(dictionary: [String: Any]) {
        
        let movieInfoDic = dictionary["movie"] as? [String: Any] ?? [:]
        
        let ids = movieInfoDic["ids"] as? [String: Any] ?? [:]
        let imdbID = ids["imdb"] as? String ?? ""
        let slug = ids["slug"] as? String ?? ""
        let tmdb = ids["tmdb"] as? String ?? ""
        let trakt = ids["trakt"] as? String ?? ""
        
        self.id = (imdb: imdbID, slug: slug, tmdb: tmdb, trakt: trakt)
        
        self.title = movieInfoDic["title"] as? String ?? ""
        self.year = movieInfoDic["year"] as? Int ?? -1
        self.type = .movie
        
        self.tagLine = movieInfoDic["tagline"] as? String
        self.overview = movieInfoDic["overview"] as? String
        self.trailer = movieInfoDic["trailer"] as? String
        self.homepage = movieInfoDic["homepage"] as? String
        self.rating = movieInfoDic["rating"] as? Double
        self.votes = movieInfoDic["votes"] as? Int
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let releaseDateString = movieInfoDic["released"] as? String {
            self.released = dateFormatter.date(from: releaseDateString)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        let imdb = aDecoder.decodeObject(forKey: "idimdb") as? String ?? ""
        let slug = aDecoder.decodeObject(forKey: "idslug") as? String ?? ""
        let tmdb = aDecoder.decodeObject(forKey: "idtmdb") as? String ?? ""
        let trakt = aDecoder.decodeObject(forKey: "idtrakt") as? String ?? ""
        
        self.id = (imdb: imdb, slug: slug, tmdb: tmdb, trakt: trakt)
        self.title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
        self.year = aDecoder.decodeObject(forKey: "year") as? Int ?? -1
        self.type = .movie
        
        self.tagLine = aDecoder.decodeObject(forKey: "tagline") as? String
        self.overview = aDecoder.decodeObject(forKey: "overview") as? String
        self.trailer = aDecoder.decodeObject(forKey: "trailer") as? String
        self.rating = aDecoder.decodeObject(forKey: "rating") as? Double
        self.votes = aDecoder.decodeObject(forKey: "votes") as? Int
        self.released = aDecoder.decodeObject(forKey: "released") as? Date
        self.homepage = aDecoder.decodeObject(forKey: "homepage") as? String

    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.id.imdb, forKey: "idimdb")
        aCoder.encode(self.id.slug, forKey: "idslug")
        aCoder.encode(self.id.tmdb, forKey: "idtmdb")
        aCoder.encode(self.id.trakt, forKey: "idtrakt")
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.year, forKey: "year")
        aCoder.encode(self.tagLine, forKey: "tagline")
        aCoder.encode(self.overview, forKey: "overview")
        aCoder.encode(self.trailer, forKey: "trailer")
        aCoder.encode(self.rating, forKey: "rating")
        aCoder.encode(self.votes, forKey: "votes")
        aCoder.encode(self.released, forKey: "released")
        aCoder.encode(self.homepage, forKey: "homepage")
        
    }
    
    var isFavorited: Bool {
        return Favorites.isFavorite(self)
    }
    
    func getPicture(quality: ImageQuality = .preview, completionHandler: @escaping ((URL?) -> Void)) {
        FanArtTVAPI.getPicture(id: self.id.imdb, quality: quality) { (url) in
            if let url = url {
                self.coverPicture = url
                completionHandler(url)
            }
        }
    }
    
}

enum ImageQuality: String {
    case fanart = "fanart"
    case preview = "preview"
}

enum ShowType: String {
    case movie = "movie"
    case tvShow = "tvShow"
}
