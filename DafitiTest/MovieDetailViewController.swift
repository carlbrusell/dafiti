//
//  MovieDetailViewController.swift
//  DafitiTest
//
//  Created by Carl Osorio on 04/09/17.
//  Copyright © 2017 Trainning. All rights reserved.
//

import UIKit
import Kingfisher
import Cosmos

class MovieDetailViewController: UIViewController {
    
    var show: ShowItem?
    
    @IBOutlet weak var movieCover: UIImageView!
    @IBOutlet weak var tagline: UILabel!
    @IBOutlet weak var grade: CosmosView!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var overview: UILabel!
    @IBOutlet weak var watchTheTrailer: UIButton!
    @IBOutlet weak var addToFavorite: UIButton!
    @IBOutlet weak var numberOfVotes: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let show = self.show else {
            return
        }
        
        self.title = show.title
        
        if show.isFavorited {
            addToFavorite.setTitle("Remover dos favoritos", for: .normal)
        }
        
        if let url = show.coverPicture {
            self.movieCover.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "PlaceholderImage"), options: [.transition(.fade(0.3))])
        } else {
            show.getPicture { (url) in
                if let url = url {
                    self.movieCover.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "PlaceholderImage"), options: [.transition(.fade(0.3))])
                }
            }
        }
        
        TracktAPI.getFullMovie(slug: show.id.slug) { (item) in
            
            self.tagline.text = item.tagLine
            self.overview.text = item.overview
            
            if let releaseDate = item.released {
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = .long
                dateFormatter.timeStyle = .none
                self.releaseDate.text = dateFormatter.string(from: releaseDate)
            }
            
            self.watchTheTrailer.isHidden = item.trailer == nil
            
            self.grade.rating = item.rating ?? 10
            self.numberOfVotes.text = "\(item.votes ?? 0) votos"
            
            self.show = item
            
        }
        
    }
    
}

// MARK: Screen Actions
extension MovieDetailViewController {

    @IBAction func watchTheTrailer(_ sender: Any) {
        if let trailer = self.show?.trailer,
            let trailerURL = URL(string: trailer),
            UIApplication.shared.canOpenURL(trailerURL) {
            UIApplication.shared.open(trailerURL, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func share(_ sender: Any) {
        
        DispatchQueue.main.async {
            let items: [Any] = [self.show?.title as Any, self.show?.homepage as Any, self.movieCover.image as Any]
            
            let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
            self.present(activityController, animated: true, completion: nil)
        }

    }
    
    @IBAction func addToFavorite(_ sender: Any) {
        if let show = self.show {
            if show.isFavorited {
                Favorites.removeFavorite(show)
                addToFavorite.setTitle("Adicionar dos favoritos", for: .normal)
            } else {
                Favorites.addToFavorite(show)
                addToFavorite.setTitle("Remover dos favoritos", for: .normal)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let zoomVC = segue.destination as? ImageZoomViewController {
            zoomVC.item = show
            zoomVC.image = movieCover.image
        }
    }
    
}
