//
//  FanArtTVAPI.swift
//  DafitiTest
//
//  Created by Carl Osorio on 03/09/17.
//  Copyright © 2017 Trainning. All rights reserved.
//

import UIKit
import Alamofire

class FanArtTVAPI: NSObject {
    
    static func getPicture(id: String, quality: ImageQuality = .preview, completionHandler: @escaping ((URL?) -> Void)) {
        
        Alamofire.request("https://webservice.fanart.tv/v3/movies/\(id)?api_key=39c3dd0382db280736ec055b838705a0").responseJSON { (answer) in
            
            if let imgDic = answer.value as? [String: Any] {
                
                if let movieClearArt = imgDic["movieposter"] as? [[String: Any]] {
                    
                    let moviesPT = movieClearArt.filter({ (dic) -> Bool in
                        
                        if let lang = dic["lang"] as? String {
                            return lang == "pt"
                        }
                        
                        return false
                        
                    })
                    
                    if let moviePT = moviesPT.first,
                        let path = moviePT["url"] as? String,
                        let url = URL(string: path.replacingOccurrences(of: "/fanart/", with: "/\(quality.rawValue)/")) {
                        
                        completionHandler(url)
                        
                    } else {
                        
                        let moviesEN = movieClearArt.filter({ (dic) -> Bool in
                            
                            if let lang = dic["lang"] as? String {
                                return lang == "en"
                            }
                            
                            return false
                            
                        })
                        
                        if let movieEN = moviesEN.first,
                            let path = movieEN["url"] as? String,
                            let url = URL(string: path.replacingOccurrences(of: "/fanart/", with: "/\(quality.rawValue)/")) {
                            completionHandler(url)
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }

}
